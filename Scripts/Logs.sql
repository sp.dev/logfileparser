drop table if exists Logs;

create table Logs ( Id int not null identity(1,1),
		[Timestamp] datetime2,
		ChannelName nvarchar(300),
		ReturnCode int,
		FileSizeInByte int,
		RequestUrl nvarchar(max),
		SessionId nvarchar(max)
		)
