drop table if exists LogsFact,
                     ReturnCode,
                     EventTime,
                     ChannelName;

create table ReturnCode (
    Id int identity(1, 1) not null primary key clustered,
    Code int not null,
    [CodeDescription] nvarchar(300)
);
insert into ReturnCode (
    Code,
    CodeDescription
)
values
     (200, 'OK'),
     (300, 'Multiple Choice'),
     (400, 'Bad Request'),
     (404, 'Not Found'),
     (500, 'Internal Server Error');


create table EventTime (
    Id int identity(1, 1) primary key clustered not null,
    [Minute] int
);

insert into EventTime (
    [Minute]
)
select distinct
       datepart(minute, l.[Timestamp])
from Logs as l
order by datepart(minute, l.[Timestamp]);


create table ChannelName (
    Id int identity(1, 1) primary key not null,
    [Name] nvarchar(300)
);

insert ChannelName (
    [Name]
)
select distinct
       l.ChannelName
from Logs as l;


create table LogsFact (
    Id int identity(1, 1) not null,
    [Timestamp] datetime2,
    EventTimeKey int foreign key references EventTime (Id) not null,
    ChannelName nvarchar(300),
    ChanelKey int foreign key references ChannelName (Id) not null,
    ReturnCodeKey int foreign key references ReturnCode (Id) not null,
    FileSizeInByte int,
    RequestUrl nvarchar(max),
    SessionId nvarchar(max)
);


insert into LogsFact (
    [Timestamp],
    EventTimeKey,
    ChannelName,
    ChanelKey,
    ReturnCodeKey,
    FileSizeInByte,
    RequestUrl,
    SessionId
)
select l.[Timestamp],
       et.Id,
       l.ChannelName,
       channel.Id,
       code.Id,
       l.FileSizeInByte,
       l.RequestUrl,
       l.SessionId
from Logs as l
    inner join EventTime as et on et.Minute = datepart(minute, l.[Timestamp])
    inner join ChannelName as channel on channel.[Name] = l.ChannelName
    inner join ReturnCode as code on code.Code = l.ReturnCode
order by l.Id;

