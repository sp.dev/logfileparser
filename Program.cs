﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Data.SqlClient;

namespace LogParser {
    class Program {
        private static string connectionString =
            "Server=localhost;Database=LogsOlap;User Id=olaptest;Password=olaptest123;";
        private static string pathToFile = AppDomain.CurrentDomain.BaseDirectory + @"access.log.1";

        static void Main(string[] args) {
            Console.WriteLine("Current path to log file is: {0}", pathToFile);

            var logs = new List<Logfile>();
            logs = ParseLog(pathToFile);
            Console.WriteLine("Number of records in log file: {0}", logs.Count);

            foreach (var log in logs) {
                LodInDb(log);
            }

            Console.WriteLine("Done.");
            Console.ReadLine();
        }

        private static List<Logfile> ParseLog(string pathToFile) {
            var logs = new List<Logfile>();
            var wrongLogsNum = 0;

            using (StreamReader reader = File.OpenText(pathToFile)) {
                var scurrentLine = String.Empty;
                while ((scurrentLine = reader.ReadLine()) != null) {
                    string[] parsedLine = scurrentLine.Split(" ");

                    if (!parsedLine[3].StartsWith("[")) {
                        wrongLogsNum++;
                        continue;
                    }

                    var log = new Logfile();
                    var dateValue = parsedLine[0] + ' ' + parsedLine[1];
                    log.Timestamp = DateTime.Parse(dateValue);
                    log.ChannelName = parsedLine[3];
                    log.ReturnCode = int.Parse(parsedLine[7]);
                    log.FileSizeInBytes = int.Parse(parsedLine[9]);
                    log.requestUrl = parsedLine[10];
                    log.sessionId = parsedLine[11];
                    logs.Add(log);
                }
            }

            Console.WriteLine("Number of skipped records due to wrong format: {0}", wrongLogsNum);
            return logs;
        }

        private static void LodInDb(Logfile log) {
            var command = new SqlCommand();
            command.CommandType = CommandType.Text;
            command.CommandText =
                "insert into Logs (Timestamp, ChannelName, ReturnCode, FileSizeInByte, RequestUrl, SessionId) values (@Timestamp, @Channel, @Code, @FileSize, @Url, @SessionId)";
            command.Parameters.AddWithValue("@Timestamp", log.Timestamp);
            command.Parameters.AddWithValue("@Channel", log.ChannelName);
            command.Parameters.AddWithValue("@Code", log.ReturnCode);
            command.Parameters.AddWithValue("@FileSize", log.FileSizeInBytes);
            command.Parameters.AddWithValue("@Url", log.requestUrl);
            command.Parameters.AddWithValue("@SessionId", log.sessionId);
            ExecuteSql(command);
        }

        private static void ExecuteSql(SqlCommand command) {
            using (SqlConnection connection = new SqlConnection(connectionString)) {
                try {
                    connection.Open();
                    command.Connection = connection;
                    command.ExecuteNonQuery();
                }
                catch (SqlException e) {
                    Console.WriteLine(e);
                }
                finally {
                    connection.Close();
                }
            }
        }
    }
}