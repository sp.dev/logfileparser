﻿using System;

namespace LogParser {
    public class Logfile {
        public DateTime Timestamp { get; set; }
        public string ChannelName { get; set; }
        public int ReturnCode { get; set; }
        public int FileSizeInBytes { get; set; }
        public string requestUrl { get; set; }
        public string sessionId { get; set; }
    }
}